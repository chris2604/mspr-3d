"use strict";

import React, { Component } from "react";

import { StyleSheet } from "react-native";

import {
  ViroARScene,
  ViroConstants,
  ViroARTrackingTargets,
  ViroARImageMarker,
  Viro3DObject,
} from "react-viro";

ViroARTrackingTargets.createTargets({
  rhinoceros: {
    source: require("./res/rhinoceros.png"),
    orientation: "Up",
    physicalWidth: 0.35, // real world width in meters
  },
  monkey: {
    source: require("./res/monkey.png"),
    orientation: "Up",
    physicalWidth: 0.35, // real world width in meters
  },
  snake: {
    source: require("./res/snake.png"),
    orientation: "Up",
    physicalWidth: 0.35, // real world width in meters
  },
});

export default class HelloWorldSceneAR extends Component {
  constructor() {
    super();

    // Set initial state here
    this.state = {
      text: "Chargement en cours...",
    };

    // bind 'this' to functions
    this._onInitialized = this._onInitialized.bind(this);
  }

  _onInitialized(state) {
    if (state == ViroConstants.TRACKING_NORMAL) {
      this.setState({
        text: "Cherche ton dessin !",
      });
    } else if (state == ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }

  render() {
    return (
      <ViroARScene onTrackingUpdated={this._onInitialized}>
        <ViroARImageMarker target={"rhinoceros"}>
          <Viro3DObject
            source={require("./res/r.obj")}
            scale={[1, 1, 1]}
            position={[0, -3, -3]}
            rotate={[0, 90, 0]}
            type="OBJ"
          />
        </ViroARImageMarker>
        <ViroARImageMarker target={"monkey"}>
          <Viro3DObject source={require("./res/m.obj")} type="OBJ" />
        </ViroARImageMarker>
        <ViroARImageMarker target={"snake"}>
          <Viro3DObject
            source={require("./res/s.obj")}
            scale={[0.2, 0.2, 0.2]}
            position={[0, -3, -3]}
            type="OBJ"
          />
        </ViroARImageMarker>
      </ViroARScene>
    );
  }
}

var styles = StyleSheet.create({
  helloWorldTextStyle: {
    fontFamily: "Arial",
    fontSize: 30,
    color: "#ffffff",
    textAlignVertical: "center",
    textAlign: "center",
  },
});

module.exports = HelloWorldSceneAR;
