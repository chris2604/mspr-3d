import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Image,
  ImageBackground,
  TouchableOpacity,
  Button,
} from "react-native";
import { ViroARSceneNavigator } from "react-viro";
import { captureScreen } from "react-native-view-shot";

import { Share } from "react-native";
var InitialARScene = require("./js/HelloWorldSceneAR");
var UNSET = "UNSET";
var AR_NAVIGATOR_TYPE = "AR";

var sharedProps = {
  apiKey: "API_KEY_HERE",
};

var defaultNavigatorType = UNSET;

export default class ViroSample extends Component {
  constructor() {
    super();

    this.state = {
      navigatorType: defaultNavigatorType,
      sharedProps: sharedProps,
      imageURI:
        "https://raw.githubusercontent.com/AboutReact/sampleresource/master/sample_img.png",
    };
    this._getExperienceSelector = this._getExperienceSelector.bind(this);
    this._getARNavigator = this._getARNavigator.bind(this);
    this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(
      this
    );
    this._exitViro = this._exitViro.bind(this);
  }

  takeScreenShot = () => {
    //handler to take screnshot
    captureScreen({
      //either png or jpg or webm (Android). Defaults to png
      format: "jpg",
      //quality 0.0 - 1.0 (default). (only available on lossy formats like jpg)
      quality: 0.8,
    }).then(
      //callback function to get the result URL of the screnshot
      (uri) => this.setState({ imageURI: uri }),
      (error) => console.error("Oops, Something Went Wrong", error)
    );
  };


  share = (url) => {
    if (Platform.OS === "ios") {
      const content = {
        message: this._message,
        title: this._title,
        url,
      };
      const options = {
        subject: this._subject,
      };
      const result = await Share.share(content, options);
      return result === Share.sharedAction;
    } else if (Platform.OS === "android") {
      const options = {
        message: this._message,
        title: this._title,
        subject: this._subject,
        url,
      };
      try {
        await AndroidShare.open(options);
        return true;
      } catch (error) {
        return false;
      }
    }
  }

  _getARNavigator() {
    return (
      <>
        <Text
          style={{
            textAlign: "center",
            alignSelf: "stretch",
            backgroundColor: "white",
            paddingTop: 30,
            paddingBottom: 10,
            zIndex: 1,
          }}
        >
          Scannez le dessin au dos du paquet de céréales
        </Text>
        <TouchableHighlight
          style={{
            position: "absolute",
            zIndex: 1,
            borderRadius: 5,
            padding: 10,
            backgroundColor: "red",
            paddingTop: 10,
            paddingBottom: 10,
            top: 60,
            left: 20,
          }}
          underlayColor={"#68a0ff"}
          onPress={this._getExperienceButtonOnPress(UNSET)}
        >
          <Text
            style={{
              fontSize: 20,
              textAlign: "center",
              color: "white",
            }}
          >
            Retour
          </Text>
        </TouchableHighlight>
        <ViroARSceneNavigator
          style={{
            zIndex: 0,
          }}
          {...this.state.sharedProps}
          initialScene={{ scene: InitialARScene }}
        />
        <View
          style={{
            height: 100,
            width: "100%",
            justifyContent: "flex-end",
            alignItems: "center",
            position: "absolute",
            bottom: 0,
          }}
        >
          <TouchableOpacity
            style={{
              alignItems: "center",
              justifyContent: "center",
              width: 100,
              height: 100,
              borderRadius: 50,
              bottom: 40,
              borderColor: "rgba(0,0,0,0.2)",
              borderWidth: 1,
            }}
          >
            <View
              style={{
                backgroundColor: "red",
                borderRadius: 100,
                width: 40,
                height: 40,
              }}
            ></View>
          </TouchableOpacity>

          <View
            style={{
              backgroundColor: "rgba(87, 151, 239, 0.1)",
              borderColor: "#5797ef",
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              borderTopWidth: 3,
              borderLeftWidth: 3,
              borderRightWidth: 3,
              paddingTop: 5,
              paddingLeft: 10,
              paddingRight: 10,
              paddingBottom: 5,
              justifyContent: "center",
              flexDirection: "row",
              width: 200,
            }}
          >
            <TouchableOpacity
              style={{
                width: 30,
                height: 30,
                marginTop: 5,
                marginBottom: 5,
                marginRight: 15,
              }}
            >
              <Image
                onPress={this.takeScreenShot}
                style={{ width: "100%", height: "100%" }}
                source={require("./js/res/socialMedia/facebook.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 30,
                height: 30,
                marginTop: 5,
                marginBottom: 5,
                marginLeft: 15,
              }}
            >
              <Image
                onPress={this.takeScreenShot}
                style={{ width: "100%", height: "100%", paddingLeft: 10 }}
                source={require("./js/res/socialMedia/instagram.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
      </>
    );
  };

  _getExperienceButtonOnPress(navigatorType) {
    return () => {
      this.setState({
        navigatorType: navigatorType,
      });
    };
  };

  _exitViro() {
    this.setState({
      navigatorType: UNSET,
    });
  }

  _getExperienceSelector() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("./js/res/background.png")}
          style={{
            flex: 1,
            resizeMode: "cover",
            justifyContent: "center",
          }}
        >
          <View style={{ width: 120, height: 120 }}>
            <Image
              style={{
                flex: 1,
                width: null,
                height: null,
                resizeMode: "contain",
              }}
              source={require("./js/res/main_logo.png")}
            />
          </View>
          <View style={{ flex: 1, alignContent: "center" }}>
            <Text
              style={{
                fontSize: 25,
                fontFamily: "Trebuchet MS",
                color: "white",
                alignContent: "center",
                textAlign: "center",
                paddingTop: 30,
                paddingBottom: 100,
                paddingLeft: 10,
              }}
            >
              CEREALIS 3D PROJECT
            </Text>
            <View
              style={{
                flex: 1,
                alignItems: "center",
                paddingTop: 180,
              }}
            >
              <TouchableHighlight
                style={{
                  borderRadius: 10,
                  padding: 20,
                  width: 200,
                  backgroundColor: "#68a0ff",
                  borderColor: "green",
                }}
                underlayColor={"#68a0ff"}
                onPress={this._getExperienceButtonOnPress(AR_NAVIGATOR_TYPE)}
              >
                <Text
                  style={{
                    fontSize: 20,
                    textAlign: "center",
                    color: "white",
                  }}
                >
                  Scanner mon dessin
                </Text>
              </TouchableHighlight>
              <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  marginTop: 5,
                  marginBottom: 5,
                  marginRight: 15,
                }}
              >
                <Image
                  source={{ uri: this.state.imageURI }}
                  style={{ width: "100%", height: "100%" }}
                />
              </TouchableOpacity>
              <Button title="Take Screenshot" onPress={this.takeScreenShot} />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }

  render() {
    if (this.state.navigatorType == UNSET) {
      return this._getExperienceSelector();
    } else if (this.state.navigatorType == AR_NAVIGATOR_TYPE) {
      return this._getARNavigator();
    }
  }
}

var localStyles = StyleSheet.create({
  viroContainer: {
    flex: 1,
    backgroundColor: "black",
  },
  outer: {
    flex: 1,
    backgroundColor: "black",
  },
  inner: {
    flex: 1,
    backgroundColor: "white",
  },
  titleText: {
    paddingTop: 30,
    paddingBottom: 20,
    color: "#fff",
    textAlign: "center",
    fontSize: 25,
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
  },
  buttons: {
    padding: 10,
    alignSelf: "flex-start",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#68a0cf",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff",
  },
  exitButton: {
    height: 50,
    width: 50,
    backgroundColor: "red",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff",
    position: "absolute",
    bottom: 0,
    left: 0,
  },
});

module.exports = ViroSample;
