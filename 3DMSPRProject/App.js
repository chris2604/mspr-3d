import React, { useState, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  ImageBackground,
  Share,
} from "react-native";
import ViewShot from "react-native-view-shot";
import { ViroARSceneNavigator } from "react-viro";

// const InitialARScene = require("./viroScene");
export default function App() {
  const [homeView, setHomeView] = useState(true);

  const [uri, setUri] = useState(require("./assets/media/main_logo.png"));
  const [screened, setScreened] = useState(false);
  const handleChangeView = () => {
    setHomeView(!homeView);
  };

  const viewShotRef = useRef("");

  const shareFacebook = async () => {
    try {
      const result = await Share.share({
        title: "Partager sur",
        message: "Un nouvel animal trouvé !!",
        url: uri,
        type: "image",
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
          alert("hello => ", result.activityType);
        } else {
          alert("else => ", result);
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
        alert("Dismissed => ", result);
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const handleScreened = () => {
    setScreened(true);
  };

  return (
    <View style={{ flex: 1 }}>
      {homeView ? (
        <ViewShot
          ref={viewShotRef}
          options={{ format: "png", quality: 0.8 }}
          style={{ flex: 1 }}
        >
          <ImageBackground
            source={require("./assets/media/background.png")}
            style={{
              flex: 1,
              resizeMode: "cover",
              justifyContent: "center",
            }}
          >
            <View style={{ width: 120, height: 120 }}>
              <Image
                style={{
                  flex: 1,
                  width: null,
                  height: null,
                  resizeMode: "contain",
                }}
                source={require("./assets/media/main_logo.png")}
              />
            </View>
            <View style={{ flex: 1, alignContent: "center" }}>
              <Text
                style={{
                  fontSize: 25,
                  color: "white",
                  alignContent: "center",
                  textAlign: "center",
                  paddingTop: 30,
                  paddingBottom: 100,
                  paddingLeft: 10,
                }}
              >
                CEREALIS 3D PROJECT
              </Text>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  paddingTop: 180,
                }}
              >
                <TouchableHighlight
                  style={{
                    borderRadius: 10,
                    padding: 20,
                    width: 200,
                    backgroundColor: "#68a0ff",
                    borderColor: "green",
                  }}
                  underlayColor={"#68a0ff"}
                  onPress={handleChangeView}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      textAlign: "center",
                      color: "white",
                    }}
                  >
                    Scanner mon dessin
                  </Text>
                </TouchableHighlight>
                <Button
                  title="Prendre une capture"
                  onPress={async () => {
                    const chemin = await viewShotRef.current.capture();
                    // alert(`require("${chemin}")`);
                    setUri(`require("${chemin}")`);
                  }}
                />
                {uri && (
                  <Image source={uri} style={{ width: 125, height: 125 }} />
                )}
              </View>
            </View>
          </ImageBackground>
        </ViewShot>
      ) : (
        <>
          <Text
            style={{
              textAlign: "center",
              alignSelf: "stretch",
              backgroundColor: "white",
              paddingTop: 30,
              paddingBottom: 10,
              zIndex: 1,
            }}
          >
            Scannez le dessin au dos du paquet de céréales
          </Text>
          <TouchableHighlight
            style={{
              position: "absolute",
              zIndex: 1,
              borderRadius: 5,
              padding: 10,
              backgroundColor: "red",
              paddingTop: 10,
              paddingBottom: 10,
              top: 60,
              left: 20,
            }}
            underlayColor={"#68a0ff"}
            onPress={handleChangeView}
          >
            <Text
              style={{
                fontSize: 20,
                textAlign: "center",
                color: "white",
              }}
            >
              Retour
            </Text>
          </TouchableHighlight>

          <View
            style={{
              height: 100,
              width: "100%",
              justifyContent: "flex-end",
              alignItems: "center",
              position: "absolute",
              bottom: 0,
            }}
          >
            <TouchableOpacity
              style={{
                alignItems: "center",
                justifyContent: "center",
                width: 100,
                height: 100,
                borderRadius: 50,
                bottom: 40,
                borderColor: "rgba(0,0,0,0.2)",
                borderWidth: 1,
              }}
            >
              <View
                style={{
                  backgroundColor: "red",
                  borderRadius: 100,
                  width: 40,
                  height: 40,
                }}
              >
                <Button title="" onPress={handleScreened} />
              </View>
            </TouchableOpacity>
            {screened && (
              <View
                style={{
                  backgroundColor: "rgba(87, 151, 239, 0.1)",
                  borderColor: "#5797ef",
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  borderTopWidth: 3,
                  borderLeftWidth: 3,
                  borderRightWidth: 3,
                  paddingTop: 5,
                  paddingLeft: 10,
                  paddingRight: 10,
                  paddingBottom: 5,
                  justifyContent: "center",
                  flexDirection: "row",
                  width: 200,
                }}
              >
                <Button onPress={shareFacebook} title="Partager" />
                {/* <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  marginTop: 5,
                  marginBottom: 5,
                  marginRight: 15,
                }}
                onPress={shareFacebook}
              >
                <Image
                  style={{ width: "100%", height: "100%" }}
                  source={require("./assets/media/socialMedia/facebook.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  marginTop: 5,
                  marginBottom: 5,
                  marginLeft: 15,
                }}
              >
                <Image
                  style={{ width: "100%", height: "100%", paddingLeft: 10 }}
                  source={require("./assets/media/socialMedia/instagram.png")}
                />
              </TouchableOpacity> */}
              </View>
            )}
          </View>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 100,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
