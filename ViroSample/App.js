import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Image,
  ImageBackground,
  TouchableOpacity
} from "react-native";

import { ViroARSceneNavigator } from "react-viro";

var InitialARScene = require("./js/HelloWorldSceneAR");
var UNSET = "UNSET";
var AR_NAVIGATOR_TYPE = "AR";

var sharedProps = {
  apiKey: "API_KEY_HERE"
};

var defaultNavigatorType = UNSET;

export default class ViroSample extends Component {
  constructor() {
    super();

    this.state = {
      navigatorType: defaultNavigatorType,
      sharedProps: sharedProps
    };
    this._getExperienceSelector = this._getExperienceSelector.bind(this);
    this._getARNavigator = this._getARNavigator.bind(this);
    this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(
      this
    );
    this._exitViro = this._exitViro.bind(this);
  }

  _getARNavigator() {
    return (
      <>
        <Text
          style={{
            textAlign: "center",
            alignSelf: "stretch",
            backgroundColor: "white",
            paddingTop: 30,
            paddingBottom: 10,
            zIndex: 1
          }}
        >
          Scannez le dessin au dos du paquet de céréales
        </Text>
        <TouchableHighlight
          style={{
            position: "absolute",
            zIndex: 1,
            borderRadius: 5,
            padding: 10,
            backgroundColor: "red",
            paddingTop: 10,
            paddingBottom: 10,
            top: 60,
            left: 20
          }}
          underlayColor={"#68a0ff"}
          onPress={this._getExperienceButtonOnPress(UNSET)}
        >
          <Text
            style={{
              fontSize: 20,
              textAlign: "center",
              color: "white"
            }}
          >
            Retour
          </Text>
        </TouchableHighlight>
        <ViroARSceneNavigator
          style={{
            zIndex: 0
          }}
          {...this.state.sharedProps}
          initialScene={{ scene: InitialARScene }}
        />
        <TouchableOpacity
          style={{
            borderWidth: 1,
            borderColor: "rgba(0,0,0,0.2)",
            alignItems: "center",
            justifyContent: "center",
            width: 100,
            height: 100,
            backgroundColor: "#fff",
            borderRadius: 50
          }}
        >
          <View
            style={{
              backgroundColor: "red",
              borderRadius: 100,
              width: 40,
              height: 40
            }}
          ></View>
        </TouchableOpacity>
      </>
    );
  }

  _getExperienceButtonOnPress(navigatorType) {
    return () => {
      this.setState({
        navigatorType: navigatorType
      });
    };
  }

  _exitViro() {
    this.setState({
      navigatorType: UNSET
    });
  }

  _getExperienceSelector() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("./js/res/background.png")}
          style={{
            flex: 1,
            resizeMode: "cover",
            justifyContent: "center"
          }}
        >
          <View style={{ width: 120, height: 120 }}>
            <Image
              style={{
                flex: 1,
                width: null,
                height: null,
                resizeMode: "contain"
              }}
              source={require("./js/res/main_logo.png")}
            />
          </View>
          <View style={{ flex: 1, alignContent: "center" }}>
            <Text
              style={{
                fontSize: 25,
                fontFamily: "Trebuchet MS",
                color: "white",
                alignContent: "center",
                textAlign: "center",
                paddingTop: 30,
                paddingBottom: 100,
                paddingLeft: 10
              }}
            >
              CEREALIS 3D PROJECT
            </Text>
            <View
              style={{
                flex: 1,
                alignItems: "center",
                paddingTop: 180
              }}
            >
              <TouchableHighlight
                style={{
                  borderRadius: 10,
                  padding: 20,
                  width: 200,
                  backgroundColor: "#68a0ff",
                  borderColor: "green"
                }}
                underlayColor={"#68a0ff"}
                onPress={this._getExperienceButtonOnPress(AR_NAVIGATOR_TYPE)}
              >
                <Text
                  style={{
                    fontSize: 20,
                    textAlign: "center",
                    color: "white"
                  }}
                >
                  Scanner mon dessin
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }

  render() {
    if (this.state.navigatorType == UNSET) {
      return this._getExperienceSelector();
    } else if (this.state.navigatorType == AR_NAVIGATOR_TYPE) {
      return this._getARNavigator();
    }
  }
}

var localStyles = StyleSheet.create({
  viroContainer: {
    flex: 1,
    backgroundColor: "black"
  },
  outer: {
    flex: 1,
    backgroundColor: "black"
  },
  inner: {
    flex: 1,
    backgroundColor: "white"
  },
  titleText: {
    paddingTop: 30,
    paddingBottom: 20,
    color: "#fff",
    textAlign: "center",
    fontSize: 25
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20
  },
  buttons: {
    padding: 10,
    alignSelf: "flex-start",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#68a0cf",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff"
  },
  exitButton: {
    height: 50,
    width: 50,
    backgroundColor: "red",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff",
    position: "absolute",
    bottom: 0,
    left: 0
  }
});

module.exports = ViroSample;
